package jClassDesigner.gui;

import jClassDesigner.controller.ComponentController;
import jClassDesigner.data.AppState;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

/**
 *
 * @author Jia Sheng Ma
 */
public class ParentConnector extends SplittableConnector {
    
    Line line;
    Polygon triangle;
    double startX, startY, endX, endY;
    
    public ParentConnector(VBox componentUI) {
        super(componentUI);
        
        startX = 50.0;
        startY = 10.0;
        endX = 50.0;
        endY = 40.0;
        
        
    }
    
//    public ParentConnector(double startX, double startY, double endX, double endY) {
//        this.startX = startX;
//        this.startY = startY;
//        this.endX = endX;
//        this.endY = endY;
//    }
    
    public Group getParentLine0() {
        //this();
        triangle = new Polygon();
        triangle.getPoints().addAll(new Double[]{
                    startX-5.0, startY,
                    startX+5.0, startY,
                    startX, 0.0 });
        line = new Line(startX, startY, endX, endY);
//        triangle.
        
        Group parentLine = new Group();
        parentLine.getChildren().addAll(triangle, line);
        return parentLine;
        
    }
    public Node getParentLine() {
        double startX, startY, endX, endY;
        startX = 50.0;
        startY = 10.0;
        endX = 50.0;
        endY = 80.0;
        
        
        Polygon triangle = new Polygon();
        triangle.getPoints().addAll(new Double[]{
                    startX-5.0, startY,
                    startX+5.0, startY,
                    startX, 0.0 });
        
        Line line = new Line(startX, startY, endX, endY);
        Group parentLine = new Group();
        parentLine.getChildren().addAll(triangle, line);
        
        //TODO: setup draggable
        
        return parentLine;
    }
    
//    public void setupDraggable(Node node) {
//        Workspace workspace = (Workspace)app.getWorkspaceComponent();
//        node.addEventFilter(MouseEvent.MOUSE_PRESSED, (final MouseEvent mouseEvent) -> {
//            if(dataManager.getAppState()==AppState.SELECT_STATE) {
//                initX = mouseEvent.getSceneX();
//                initY = mouseEvent.getSceneY();
//                init_translateX = node.getTranslateX();
//                init_translateY = node.getTranslateY();
//                dataManager.setSelectedNode(node);
//                
//                // CLEAR COMPONENT TOOLBAR PANE
//                workspace.getComponentToolbarPane().getChildren().clear();
//        
//                // ADD SELECTED DIAGRAM'S COMPOENENT TOOLBAR PANE
//                VBox ui = dataManager.getComponentUI((VBox)node);
//                workspace.getComponentToolbarPane().getChildren().add(ui);
//                workspace.getComponentToolbarPane().setDisable(false);
//            }
//        });
//
//        node.addEventFilter(MouseEvent.MOUSE_DRAGGED, (final MouseEvent mouseEvent) -> {
//            if(dataManager.getAppState()==AppState.SELECT_STATE) {
//                node.setTranslateX(init_translateX + mouseEvent.getSceneX() - initX);
//                node.setTranslateY(init_translateY + mouseEvent.getSceneY() - initY);
//// DEBUG
////                System.out.println("layout x: " + node.getLayoutX()+ "\nlayout y: " + node.getLayoutY());
//                
//                // SET TRANSLATE X AND Y OF NODE
//                
//                // UPDATE TOOLBAR CONTROL
//                app.getGUI().updateToolbarControls(false);
//            }
//            
//        });
//    }
    
}
