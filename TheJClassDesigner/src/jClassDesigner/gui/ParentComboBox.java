package jClassDesigner.gui;

import jClassDesigner.data.DataManager;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

/**
 * This class contains all the available classes the designer created 
 * in a choice box so that the designer can choose appropriate classes
 * for the use of inheritance.
 * 
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class ParentComboBox extends ComboBox {
    /*AppTemplate app;*/
    DataManager dataManager;
    public static ObservableList<String> parentClasses;
    List<String> parentClassNames;
//    AddClassUI classUI;
    VBox componentUI;
    public ParentComboBox(/*AppTemplate app*/VBox componentUI) {
        /*this.app = app;
        dataManager = (DataManager)app.getDataComponent();*/
        
        this.componentUI = componentUI;
        
        dataManager = new DataManager();
        parentClassNames = dataManager.getClassNames();
        parentClasses = FXCollections.observableArrayList(parentClassNames);
        this.setItems(parentClasses);
    }
    
    /**
     * Add available classes to the parent_cbb for each class to choose parent class.
     * @return parent choice box that contains available classes for this class to extend.
     */
    public ObservableList<String> getParentClasses() {
        return parentClasses;
    }
    public void addParent(String parentName) {
        parentClasses.add(parentName);
        refresh();
    }
    public void setParents(List<String> parents) {
        parentClasses.clear();
        // ADD EVERY OTHER CLASSES AS AVAILABLE PARENTS EXCEPT FOR THIS
        if(componentUI instanceof AddClassUI) {
            parents.remove(((AddClassUI)componentUI).getClassName_tf().getText());
        } else {
            parents.remove(((AddInterfaceUI)componentUI).getInterfaceName_tf().getText());
        }
        parentClasses = FXCollections.observableArrayList(parents);
        refresh();
    }
    public void refresh(/*DataManager dataManger*/) {
        this.setItems(parentClasses);
    }
}
