
package jClassDesigner.gui;

import jClassDesigner.controller.ComponentController;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author Jia Sheng Ma
 */
public class SplittableConnector extends Line {
    public final double RADIUS = 5.0;
    public final double LINE_WIDTH = 4.0;
    public final double INIT_ANCHORX = 20;
    public final double INIT_ANCHORY = 100;
    DoubleProperty anchorX;
    DoubleProperty anchorY;
    
    // CREATE 2 CIRCLE AS THE TWO ENDS OF THE LINE SEGMENT
    // WHEN KEY "S" IS PRESS WHILE THIS LINE IS SELECTED, 
    // THIS LINE SHOULD BE SPLITTED IN THE MIDDLE BY ADDING A CIRCLE IN THE MIDDLE
    // WHEN THE CIRCLES ARE BEING DRAGGED, THE LINE SHOULD CHANGE ACCORDINGLY
    
    ComponentController componentController;
    Anchor anchor;
    VBox componentUI;
    
    public SplittableConnector(VBox componentUI) {
        
        // THIS STATES WHICH CLASS/INTERFACE DOES THE CONNECTOR PERTAIN TO 
        this.componentUI = componentUI;
        
        componentController = new ComponentController();
        anchorX = new SimpleDoubleProperty(INIT_ANCHORX);
        anchorY = new SimpleDoubleProperty(INIT_ANCHORY);
        
        anchor = new Anchor(anchorX, anchorY);
        
        // SET UP DRAGGABLE
        componentController.setupDraggable(this);
        
    }
    
    public void split(double startX, double startY, double endX, double endY) {
        double centerX = (startX + endX) / 2;
        double centerY = (startY + endY) / 2;
        DoubleProperty centerX_DoubleProperty = new SimpleDoubleProperty(centerX);
        DoubleProperty centerY_DoubleProperty = new SimpleDoubleProperty(centerY);
        System.out.println("not supported yet");
    }
    
    public void merge(double startX, double startY, double endX, double endY) {
        System.out.println("not supported yet");
    }
    /**
     * Anchor/point 
     */
    private class Anchor extends Circle {
        public Anchor(DoubleProperty x, DoubleProperty y) {
            super(x.get(), y.get(), RADIUS, Color.BLACK);
            x.bind(centerXProperty());
            y.bind(centerYProperty());
           
            // SET UP DRAGGABLE
        }
        
    }
}
