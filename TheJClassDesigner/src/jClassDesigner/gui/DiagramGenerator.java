package jClassDesigner.gui;

import jClassDesigner.Constants;
import java.util.ArrayList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Jia Sheng Ma
 * @version 1.1
 */
public class DiagramGenerator extends VBox{
    
    private double initX;
    private double initY;
    private double init_translateX;
    private double init_translateY;
    
    Label className;
    
    ArrayList<Variable> variables_var;
    ArrayList<Label> variables_lb;
    ArrayList<Label> methods_lb;
    VBox classBox;
    VBox variableBox;
    VBox methodBox;
    
    public DiagramGenerator() {
        //super();
        
        variables_lb = new ArrayList<>();
        methods_lb = new ArrayList<>();
        className = new Label(Constants.DEFAULT_CLASS_NAME);
        classBox = new VBox();
        classBox.getChildren().add(className);
        this.getChildren().add(classBox);
        
        variableBox = new VBox();
        methodBox = new VBox();
        
        // ADD THE CONTAINERS TO THE DIAGRAM
        this.getChildren().add(variableBox);
        this.getChildren().add(methodBox);
        
        this.
        
        initStyle();
    }
    
    /**
     * For adding variables_lb.
     * @return the box for adding variables_lb.
     */
    public VBox getVariableBox() {
        return variableBox;
    }
    /**
     * For adding methods_lb.
     * @return the box for adding methods_lb.
     */
    public VBox getMethoBox() {
        return methodBox;
    }
    
    public void addVariable(Variable variable) {
        Label varInfo = variable.getVariableInfo();
        variables_lb.add(varInfo);
        variableBox.getChildren().add(varInfo);
    }
    public void addMethod(Method method) {
//        Label 
//        methodBox.getChildren().add(method);
    }
    
    /**
     * For creating diagrams in loading pre-existing info.
     * @param name class name
     * @param layoutX layout x of diagram
     * @param layoutY layout y of diagram
     */
    public DiagramGenerator(String name, double layoutX, double layoutY) {
        
        initStyle();
    }
    
    /**
     * Changes the class name in the diagram.
     * @param name class name of the diagram.
     */
    public void editClassName(String name) {
//        if(!name.trim().equals("")) {
            className.setText(name);
//        }
//        else {
//            Dialog alert = new Alert(Alert.AlertType.WARNING, "Class Name Cannot be Empty", ButtonType.OK);
//            alert.show();
//        }
    }
    
    public void editVariable(String varInfo, int varIndexInDiagram) {
        variables_lb.get(varIndexInDiagram).setText(varInfo);
    }
    
    public void editMethod(Label lb, String s) {
        lb.setText(s);
    }
    
    /**
     * @return the Class name of this UML class diagram.
     */
    public String getDiagramClassName() {
        return className.getText().trim();
    }
    
    /**
     * Used for loading in the diagrams only. 
     * Sets up the location of the diagram to where is was last saved.
     * @param translateX amount of x translation of the diagram.
     * @param translateY amount of y translation location of the diagram.
     */
    public void setupLocation(double translateX, double translateY) {
        this.setTranslateX(translateX);
        this.setTranslateY(translateY);
    }
    
    /**
     * Refreshes this diagram on the workspace 
     * (usually called when components are readied).
     */
    public void refresh() {
        
    }
    
    /**
     * Sets up the style of the diagrams.
     */
    public void initStyle() {
        this.setMinSize(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT);
        this.getStyleClass().add(Constants.DIAGRAM);
        classBox.getStyleClass().add(Constants.DIAGRAM_COMPONENTS);
        variableBox.getStyleClass().add(Constants.DIAGRAM_COMPONENTS);
        methodBox.getStyleClass().add(Constants.DIAGRAM_COMPONENTS);
        variableBox.setMinSize(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT/2);
        methodBox.setMinSize(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT/2);
        //className.setAlignment(Pos.CENTER);
    }
    
}
