/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jClassDesigner.gui;

import jClassDesigner.Constants;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author majiasheng
 */
public class Method extends HBox{
    private TextField name;
    private TextField returnType;
    private CheckBox staticStatus;
    private CheckBox abstractStatus;
    private ComboBox access;
    private ArrayList<TextField> argTypes;
    private ArrayList<String> argNames;
    private String argNamePrefix = "arg";
    private TextField arg1;
    private Button argAdd_btn;
    private Button argRemove_btn;
    private int argNameCounter = 0;
    
    private Label methodInfo;
    
    private final ObservableList<String> accessModifiers = FXCollections.observableArrayList(Constants.PUBLIC, Constants.PROTECTED, Constants.PRIVATE);
    
    public Method(VBox componentUI) {
        super();
        name = new TextField();
        returnType = new TextField();
        staticStatus = new CheckBox();
        abstractStatus = new CheckBox();
        access = new ComboBox(accessModifiers);
        argTypes = new ArrayList<>();
        argNames = new ArrayList<>();
        arg1 = new TextField();
        
        // TODO: IMPROVE ARGUMENT ADDING/REMOVING
        // argTypes.add(arg1);
        
        argAdd_btn = new Button("+arg");
        argRemove_btn = new Button("-arg");
        this.getChildren().addAll(name, returnType, staticStatus, abstractStatus, access, arg1/*, argAdd_btn, argRemove_btn*/);
        initStyle();
        
    }
    
    public void setupHandler() {

        name.setOnAction(e -> {
            handleEditMethodName();
        });
        returnType.setOnAction(e -> {
            handleEditReturnType();
        });
        staticStatus.setOnAction(e -> {
            handleEditMethodStatic();
        });
        abstractStatus.setOnAction(e ->{
            handleEditAbstractStatus();
        });
        access.setOnAction(e -> {
            handleEditMethodAccess();
        });
        
        argAdd_btn.setOnAction(e -> {
            handleAddArg();
        });
        argRemove_btn.setOnAction(e -> {
            handleRemoveArg();
        });
        
    }

    /**
     * This method receives the value of name.
     * @return the value of name.
     */
    public TextField getName(){
        return name;
    }

    /**
     * This method receives the value of returnType.
     * @return the value of returnType.
     */
    public TextField getReturnType(){
        return returnType;
    }
    
    /**
     * This method receives the value of staticStatus.
     * @return the value of staticStatus.
     */
    public CheckBox getStaticStatus(){
        return staticStatus;
    }

    /**
     * This method receives the value of abstractStatus.
     * @return the value of abstractStatus.
     */
    public CheckBox getAbstractStatus(){
        return abstractStatus;
    }
    
    /**
     * This method receives the value of access.
     * @return the value of access.
     */
    public ComboBox getAccess(){
        return access;
    }
    
    /**
     * This method receives the value of argTypes.
     * @return the value of argTypes.
     */
    public ArrayList<TextField> getArgTypes(){
        return argTypes;
    }
    public ArrayList<String> getArgNames() {
        return argNames;
    }

    public void addArgType(TextField argType) {
        argTypes.add(argType);
//        TextField arg = new TextField();
//        this.getChildren().add(arg);
    }
    
    public void removeArgument() {
        
    }
    
    public void refreshMethodInfo() {
        
    }
    
    public Label getMethodInfo() {
        refreshMethodInfo();
        return methodInfo;
    }
    
    public void initStyle() {
        this.setSpacing(Constants.SPACING);
        this.setPrefWidth(Constants.VAR_METHOD_WIDTH);
        this.getStyleClass().add(Constants.HBOXES);
        
        name.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        returnType.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        staticStatus.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        abstractStatus.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        access.setPrefWidth(Constants.METHOD_BOX_WIDTH);
        arg1.setPrefWidth(Constants.METHOD_BOX_WIDTH);
    }

    private void handleEditMethodName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditReturnType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditMethodStatic() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditAbstractStatus() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleEditMethodAccess() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void handleAddArg() {
        TextField newArg = new TextField();
        argTypes.add(newArg);
        argNameCounter++;
        argNames.add(argNamePrefix+argNameCounter);
        int size = this.getChildren().size();
        this.getChildren().add(size-1-2, newArg);
    }

    private void handleRemoveArg() {
        // SAVE ONE ARGUMENT AS DEFAULT NUMBER
        if(argTypes.size() < 1) {
            // REMOVE THE RIGHTMOST ARG
            argTypes.remove(argTypes.size()-1);
            argNames.remove(argNames.size()-1);
            argNameCounter--;
            int size = this.getChildren().size();
            this.getChildren().remove(size-1-2);
        }
        
    }
    
}
