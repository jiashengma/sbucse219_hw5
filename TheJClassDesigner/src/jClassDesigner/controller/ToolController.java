package jClassDesigner.controller;

import jClassDesigner.data.AppState;
import jClassDesigner.data.DataManager;
import jClassDesigner.file.FileManager;
import jClassDesigner.gui.AddClassUI;
import jClassDesigner.gui.AddInterfaceUI;
import jClassDesigner.gui.DiagramGenerator;
import jClassDesigner.gui.Workspace;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Screen;
import saf.AppTemplate;
import saf.ui.AppGUI;

/**
 * This class responds to interactions with the rendering surface.
 * 
 * @author Jia Sheng Ma
 * @version 1.0
 */
public class ToolController {
    AppTemplate app;
    DataManager dataManager;
    AppState state;
    AppGUI gui;
    
    public ToolController(AppTemplate initApp) {
	app = initApp;
        gui = app.getGUI();
        dataManager = (DataManager)app.getDataComponent();
    }

    public void handleSelect() {
        dataManager.setAppState(AppState.SELECT_STATE);
    }

    public void handleResize() {
        dataManager.setAppState(AppState.RESIZE_STATE);
    }

    /**
     * Removes selected class diagram and its corresponding component ui.
     */
    public void handleRemove() {
        VBox selectedNode = (VBox)dataManager.getSelectedNode();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        if(dataManager.getSelectedNode() != null) {
            // REMOVE DIAGRAM FROM DATA MANAGER 
            dataManager.getDiagrams().remove(selectedNode);
            // AND FROM WORK PANE
            workspace.getWorkPane().getChildren().remove((DiagramGenerator)selectedNode);
            
            // REMOVE ITS CORRESPONDING COMPONENT UI FROM DATA MANAGER 
            dataManager.getComponentUIs().remove(selectedNode);
            // AND FROM COMPONENT TOOL BAR
            workspace.getComponentToolbarPane().getChildren().clear();
        }
        // MARK AS UNSAVED
        app.getGUI().updateToolbarControls(false);
        
    }

    public void handleUndo() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        /*DataManager */dataManager = (DataManager)app.getDataComponent();
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleRedo() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        /*DataManager */dataManager = (DataManager)app.getDataComponent();
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleZoomIn() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        //workspace.getWorkPane().setScaleX();
    }

    public void handleZoomOut() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        //workspace.getWorkPane().setScaleX();
    }

    public void handleGridCheck(CheckBox grid_cbx) {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        if(grid_cbx.selectedProperty().getValue()) {
            for (int i = 0; i < Screen.getPrimary().getBounds().getHeight(); i+=10) {
                Line top_bottom = new Line(0, i, Screen.getPrimary().getBounds().getWidth(), i);
                top_bottom.setStroke(Color.BLACK);
                
                //FIXME
                workspace.getWorkPane().getChildren().add(0, top_bottom);
            }
            for (int i = 0; i < Screen.getPrimary().getBounds().getWidth(); i+=10) {
                Line left_right = new Line(i, 0, i, Screen.getPrimary().getBounds().getHeight());
                left_right.setStroke(Color.BLACK);
                
                //FIXME
                workspace.getWorkPane().getChildren().add(1, left_right);
            }
        } else {
            //FIXME
            workspace.getWorkPane().getChildren().remove(0);
            workspace.getWorkPane().getChildren().remove(1);
        }
        
        app.getGUI().updateToolbarControls(false);
    }

    public void handleSnapCheck() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        app.getGUI().updateToolbarControls(false);
        
    }

    public void handleExportPhotoRequest() {
        
    }
    
    /**
     * Exports UML design to code.
     */
    public void handleExportCodeRequest() {
        System.out.println("Start exporting code\n>>>");
        // GORUP PACKAGE AND CODE FOR EXPORTING
        dataManager.groupPackageAndCode();
        
        // GET THE LIST OF CLASSNAMES TO NAME THE FILE
        Collection<VBox> uis = dataManager.getComponentUIs().values();
        ArrayList<String> classNames = new ArrayList<>();
        for(VBox ui : uis) {
            String fileName = "";
            if(ui instanceof AddClassUI) {
                fileName+=((AddClassUI)ui).getClassName_tf().getText();
            } else if(ui instanceof AddInterfaceUI) {
                fileName += ((AddInterfaceUI)ui).getInterfaceName_tf().getText();
            }
            fileName+=".java";
            classNames.add(fileName);
            // DEBUG: 
            //System.out.println("file name: " + fileName);
        }
        
        // GET DIRECTORIES PATH ARRAY
        ArrayList<String> packageSets = dataManager.getPackageNameForExport();
        
        // GET CODE ARRAY
        ArrayList<String> codes = dataManager.getCodeForExport();
        boolean exportSuccess = false;
        // OUTPUT VALUE(Code) TO KEY(Directories)
        for(int i = 0; i < codes.size(); i++) {
            String path = FileManager.CODE_PATH;
            path += (packageSets.get(i));
            
            File dir = new File(path);
                // FIRST CREATE DIRECTORIES
            System.out.println("Directories created: " + dir.mkdirs());

            try {
                // path should be path + class name.java
                String filePath = path + classNames.get(i);
                PrintWriter pw = new PrintWriter(filePath);
                pw.print(codes.get(i));
                pw.close();
                
                // NOTIFY THE USERS ONCE DONE EXPORTING
                exportSuccess = true;
            } catch(FileNotFoundException fnfe) {
                Dialog alert = new Alert(Alert.AlertType.ERROR, "ERROR IN WRITING TO FILE", ButtonType.OK);
                alert.show();
            }
        
        }
        if(exportSuccess) {
            Dialog alert = new Alert(Alert.AlertType.INFORMATION, "Exported code successfully.", ButtonType.OK);
            alert.show();
        }
        
        System.out.println("<<<\nExporting code terminated.");
    }
    
}
