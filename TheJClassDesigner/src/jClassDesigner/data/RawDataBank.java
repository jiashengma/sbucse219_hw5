package jClassDesigner.data;

import jClassDesigner.Constants;
import jClassDesigner.gui.AddClassUI;
import jClassDesigner.gui.AddInterfaceUI;
import jClassDesigner.gui.DiagramGenerator;
import jClassDesigner.gui.Method;
import jClassDesigner.gui.Variable;
import jClassDesigner.gui.Workspace;
import java.util.ArrayList;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import saf.AppTemplate;

/**
 *
 * @author Jia Sheng Ma
 */
public class RawDataBank {

    public static ArrayList<ComponentUI_DataWrapper> data = new ArrayList<>();;   
    
    public RawDataBank() {
    }
    
    /**
     * Called when save data, 
     * @param dataManager 
     */
    public static void extractDataFromDataManager(DataManager dataManager) {
        // CLEAR OLD DATA TO SAVE NEW DATA
        data.clear();
        
        // EXTRACT NEW DATA
        for(VBox v : dataManager.getComponentUIs().values()) {
            if(v instanceof AddClassUI) {
                AddClassUI c = (AddClassUI)v;
                ComponentUI_DataWrapper ui = new ComponentUI_DataWrapper(Constants.CLASS);
                ui.setName(c.getClassName());
                ui.setPackageName(c.getPackageName());
                ui.setParent(c.getparent());
                ui.setTranslateX(c.getDiagram().getTranslateX());
                ui.setTranslateY(c.getDiagram().getTranslateY());
                // Variable data
                for(Variable var: c.getVariables()) {
                    Variable_DataWrapper variable = new Variable_DataWrapper();
                    variable.setName(var.getName().getText());
                    variable.setType(var.getType().getText());
                    variable.setStatic(var.getStaticStatus().selectedProperty().getValue());
                    variable.setAccess(var.getAccess().getValue().toString());
                    
                    ui.addVariable(variable);
                }
                
                // Method data
                for(Method m : c.getMethods()) {
                    Method_DataWrapper method = new Method_DataWrapper();
                
                    method.setName(m.getName().getText());
                    method.setReturnType(m.getReturnType().getText());
                    method.setStatic(m.getStaticStatus().selectedProperty().getValue());
                    // IF THERE'S AN ABSTRACT METHOD, THE CLASS IS ABSTRACT
                    if(m.getAbstractStatus().selectedProperty().getValue()) {
                        ui.setType(Constants.ABSTRACT_CLASS);
                    }
                    method.setAbstract(m.getAbstractStatus().selectedProperty().getValue());
                    method.setAccess(m.getAccess().getValue().toString());
                    for(TextField tf : m.getArgTypes()) {
                        method.addArgument(tf.getText());
                    }
                    ui.addMethod(method);
                }
                // ADD DATA TO LIST
                data.add(ui);
                
            } // END OF INSTANCE OF CLASS
            else {
                AddInterfaceUI it = (AddInterfaceUI)v;
                ComponentUI_DataWrapper ui = new ComponentUI_DataWrapper(Constants.INTERFACE);
                ui.setName(it.getInterfaceName());
                ui.setPackageName(it.getPackageName());
                ui.setParent(it.getparent());
                ui.setTranslateX(it.getDiagram().getTranslateX());
                ui.setTranslateY(it.getDiagram().getTranslateY());
                // Variable data
                for(Variable var: it.getVariables()) {
                    Variable_DataWrapper variable = new Variable_DataWrapper();
                    variable.setName(var.getName().getText());
                    variable.setType(var.getType().getText());
                    variable.setStatic(var.getStaticStatus().selectedProperty().getValue());
                    variable.setAccess(var.getAccess().getValue().toString());
                    
                    ui.addVariable(variable);
                }
                
                // Method data
                for(Method m : it.getMethods()) {
                    Method_DataWrapper method = new Method_DataWrapper();
                
                    method.setName(m.getName().getText());
                    method.setReturnType(m.getReturnType().getText());
                    method.setStatic(m.getStaticStatus().selectedProperty().getValue());
                    method.setAbstract(m.getAbstractStatus().selectedProperty().getValue());
                    method.setAccess(m.getAccess().getValue().toString());
                    for(TextField tf : m.getArgTypes()) {
                        method.addArgument(tf.getText());
                    }
                    ui.addMethod(method);
                }
                
                // ADD DATA TO LIST
                data.add(ui);
            } // END OF INSTANCE OF INTERAFCE
        }
    }
    
    /**
     * Create objects with given data in data bank.
     * (Only reloadWorkspace() method should call this method.)
     * @param app
     * @param workspace 
     * @param dataManager 
     */
    public static void createObjects(AppTemplate app, Workspace workspace, DataManager dataManager) {
        // TODO: CLEAR OLD DATA IN DATA MANAGER 
        //       RELOAD DATA TO DATA MANAGER 
        
        for(int i = 0; i < data.size(); i++) {
            // ** CREATE OBJECT **//
            if(data.get(i).getType().equals(Constants.CLASS)) {
                AddClassUI classUI = new AddClassUI(app);
                // TODO: create everything else, add to containers
                classUI.getClassName_tf().setText(data.get(i).getName());
                classUI.getPackageName_tf().setText(data.get(i).getPackage());
                classUI.getParentComboBox().setValue(data.get(i).getParent());
                // VARIABLE
                for(int j = 0; j < data.get(i).getVariables().size(); j++) {
                    Variable_DataWrapper variable_data = data.get(i).getVariables().get(j);
                    Variable variable = new Variable(classUI);
                    variable.getName().setText(variable_data.getName());
                    variable.getType().setText(variable_data.getType());
                    variable.getStaticStatus().selectedProperty().set(variable_data.getVariableStatic());
                    variable.getAccess().setValue(variable_data.getVariableAccess());
                    // ADD VARIABLE TO LIST
                    classUI.addVariableToList(variable);
                }
                // METHOD
                for(int k = 0; k < data.get(i).getMethods().size(); k++) {
                    Method_DataWrapper method_data = data.get(i).getMethods().get(k);
                    Method method = new Method(classUI);
                    method.getName().setText(method_data.getName());
                    method.getReturnType().setText(method_data.getReturnType());
                    method.getStaticStatus().selectedProperty().set(method_data.getMethodStatic());
                    method.getAbstractStatus().selectedProperty().set(method_data.getAbstract());
                    method.getAccess().setValue(method_data.getAccess());
                    
                    for(int l = 0; l < method_data.getArguments().size(); l++) {
                        method.getArgTypes().add(new TextField((method_data.getArguments()).get(l)));
                        //((method.getArgTypes()).get(l)).setText((method_data.getArguments()).get(l));
                    }
                    // ADD METHOD TO LIST
                    classUI.addMethodToList(method);
                }
                DiagramGenerator diagram = classUI.getDiagram();
                diagram.editClassName(data.get(i).getName());
                diagram.setTranslateX(data.get(i).getTranslateX());
                diagram.setTranslateY(data.get(i).getTranslateY());
                // ** ADD TO DATAMANAGER ** //
                dataManager.addComponentUI(diagram, classUI);
                
                // ** ADD TO WORKSPACE **//
                workspace.getWorkPane().getChildren().add(diagram);
                
                
            } // end of if Constants.CLASS 

            // CREATE OBJECT
            else if(data.get(i).getType().equals(Constants.INTERFACE)) {
                AddInterfaceUI interfaceUI = new AddInterfaceUI(app);
                // TODO: create everything else, add to containers
                interfaceUI.getInterfaceName_tf().setText(data.get(i).getName());
                interfaceUI.getPackageName_tf().setText(data.get(i).getPackage());
                interfaceUI.getParentComboBox().setValue(data.get(i).getParent());
                // VARIABLE
                for(int j = 0; j < data.get(i).getVariables().size(); j++) {
                    Variable_DataWrapper variable_data = data.get(i).getVariables().get(j);
                    Variable variable = new Variable(interfaceUI);
                    variable.getName().setText(variable_data.getName());
                    variable.getType().setText(variable_data.getType());
                    variable.getStaticStatus().selectedProperty().set(variable_data.getVariableStatic());
                    variable.getAccess().setValue(variable_data.getVariableAccess());
                    // ADD VARIABLE TO LIST
                    interfaceUI.addVariableToList(variable);
                }
                // METHOD
                for(int k = 0; k < data.get(i).getMethods().size(); k++) {
                    Method_DataWrapper method_data = data.get(i).getMethods().get(k);
                    Method method = new Method(interfaceUI);
                    method.getName().setText(method_data.getName());
                    method.getReturnType().setText(method_data.getReturnType());
                    method.getStaticStatus().selectedProperty().set(method_data.getMethodStatic());
                    method.getAbstractStatus().selectedProperty().set(method_data.getAbstract());
                    method.getAccess().setValue(method_data.getAccess());
                    
                    for(int l = 0; l < method_data.getArguments().size(); l++) {
                        method.getArgTypes().add(new TextField((method_data.getArguments()).get(l)));
                    }
                    // ADD METHOD TO LIST
                    interfaceUI.addMethodToList(method);
                }
                DiagramGenerator diagram = interfaceUI.getDiagram();
                diagram.editClassName(data.get(i).getName());
                diagram.setTranslateX(data.get(i).getTranslateX());
                diagram.setTranslateY(data.get(i).getTranslateY());
                // ** ADD TO DATAMANAGER ** //
                dataManager.addComponentUI(diagram, interfaceUI);
                
                // ** ADD TO WORKSPACE **//
                workspace.getWorkPane().getChildren().add(diagram);
                
            } // end of if Constants.INTERFACE
            
        }
        System.out.println("Reloading workspace...\nFinished creating objects.");
    }
    
}
